<?php

namespace App\Controller;

use App\Entity\Examplar;
use App\Form\ExamplarType;
use App\Repository\ExamplarRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/examplar")
 */
class ExamplarController extends AbstractController
{
    /**
     * @Route("/", name="examplar_index", methods={"GET"})
     */
    public function index(ExamplarRepository $examplarRepository): Response
    {
        return $this->render('examplar/index.html.twig', [
            'examplars' => $examplarRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="examplar_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $examplar = new Examplar();
        $form = $this->createForm(ExamplarType::class, $examplar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($examplar);
            $entityManager->flush();

            return $this->redirectToRoute('examplar_index');
        }

        return $this->render('examplar/new.html.twig', [
            'examplar' => $examplar,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="examplar_show", methods={"GET"})
     */
    public function show(Examplar $examplar): Response
    {
        return $this->render('examplar/show.html.twig', [
            'examplar' => $examplar,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="examplar_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Examplar $examplar): Response
    {
        $form = $this->createForm(ExamplarType::class, $examplar);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('examplar_index');
        }

        return $this->render('examplar/edit.html.twig', [
            'examplar' => $examplar,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="examplar_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Examplar $examplar): Response
    {
        if ($this->isCsrfTokenValid('delete'.$examplar->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($examplar);
            $entityManager->flush();
        }

        return $this->redirectToRoute('examplar_index');
    }
}
