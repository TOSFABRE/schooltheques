<?php

namespace App\Controller;

use App\Entity\Lease;
use App\Form\LeaseType;
use App\Form\LeaseEditType;
use App\Repository\LeaseRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/lease")
 */
class LeaseController extends AbstractController
{
    /**
     * @Route("/", name="lease_index", methods={"GET"})
     * @param LeaseRepository $leaseRepository
     * @return Response
     */
    public function index(LeaseRepository $leaseRepository): Response
    {
        return $this->render('lease/index.html.twig', [
            'leases' => $leaseRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="lease_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $lease = new Lease();
        $form = $this->createForm(LeaseType::class, $lease);
        $form->handleRequest($request);


        if ($form->isSubmitted() && $form->isValid()) {
            $lease->setUser($this->getUser());
            $leaseid = $form['student']->getData()->getId();
            $leaseCount = count($this->getDoctrine()->getRepository(Lease::class)->findBy(['end' => null, 'student' => $leaseid]));
            if ($leaseCount < 2 )
            {
                $entityManager = $this->getDoctrine()->getManager();
                $entityManager->persist($lease);
                $entityManager->flush();
                $this->addFlash('success', 'Location éffectuée');
            }
            else
            {
                $this->addFlash('error', 'Deux locations en cours');
                return $this->redirectToRoute('lease_new');
            }

            return $this->redirectToRoute('lease_index');
        }

        return $this->render('lease/new.html.twig', [
            'lease' => $lease,
            'form' => $form->createView()
        ]);
    }

    /**
     * @Route("/{id}", name="lease_show", methods={"GET"})
     */
    public function show(Lease $lease): Response
    {
        return $this->render('lease/show.html.twig', [
            'lease' => $lease,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="lease_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Lease $lease): Response
    {
        $form = $this->createForm(LeaseEditType::class, $lease);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $leaseend = $form['end']->getData();
            $leasebegin = $lease->getBegin();
            if ($leasebegin < $leaseend)
            {
                $this->getDoctrine()->getManager()->flush();
                $this->addFlash('success', 'Locations terminer');
                return $this->redirectToRoute('lease_index');
            }
            else
            {
                $this->addFlash('error', 'La date doit être supérieur à la date de location');
            }
        }

        return $this->render('lease/edit.html.twig', [
            'lease' => $lease,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="lease_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Lease $lease): Response
    {
        if ($this->isCsrfTokenValid('delete'.$lease->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($lease);
            $entityManager->flush();
        }

        return $this->redirectToRoute('lease_index');
    }
}
