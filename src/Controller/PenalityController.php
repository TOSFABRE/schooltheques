<?php

namespace App\Controller;

use App\Entity\Penality;
use App\Form\PenalityType;
use App\Repository\PenalityRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * @Route("/penality")
 */
class PenalityController extends AbstractController
{
    /**
     * @Route("/", name="penality_index", methods={"GET"})
     */
    public function index(PenalityRepository $penalityRepository): Response
    {
        return $this->render('penality/index.html.twig', [
            'penalities' => $penalityRepository->findAll(),
        ]);
    }

    /**
     * @Route("/new", name="penality_new", methods={"GET","POST"})
     */
    public function new(Request $request): Response
    {
        $penality = new Penality();
        $form = $this->createForm(PenalityType::class, $penality);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->persist($penality);
            $entityManager->flush();

            return $this->redirectToRoute('penality_index');
        }

        return $this->render('penality/new.html.twig', [
            'penality' => $penality,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="penality_show", methods={"GET"})
     */
    public function show(Penality $penality): Response
    {
        return $this->render('penality/show.html.twig', [
            'penality' => $penality,
        ]);
    }

    /**
     * @Route("/{id}/edit", name="penality_edit", methods={"GET","POST"})
     */
    public function edit(Request $request, Penality $penality): Response
    {
        $form = $this->createForm(PenalityType::class, $penality);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->getDoctrine()->getManager()->flush();

            return $this->redirectToRoute('penality_index');
        }

        return $this->render('penality/edit.html.twig', [
            'penality' => $penality,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/{id}", name="penality_delete", methods={"DELETE"})
     */
    public function delete(Request $request, Penality $penality): Response
    {
        if ($this->isCsrfTokenValid('delete'.$penality->getId(), $request->request->get('_token'))) {
            $entityManager = $this->getDoctrine()->getManager();
            $entityManager->remove($penality);
            $entityManager->flush();
        }

        return $this->redirectToRoute('penality_index');
    }
}
