<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200526154805 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE book (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(40) NOT NULL, author VARCHAR(60) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE examplar (id INT AUTO_INCREMENT NOT NULL, book_id INT NOT NULL, isbn VARCHAR(20) NOT NULL, created_at DATETIME NOT NULL, INDEX IDX_68BB58D716A2B381 (book_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE lease (id INT AUTO_INCREMENT NOT NULL, student_id INT NOT NULL, user_id INT NOT NULL, examplar_id INT NOT NULL, begin DATE NOT NULL, end DATE NULL, created_at DATETIME NOT NULL, INDEX IDX_E6C77495CB944F1A (student_id), INDEX IDX_E6C77495A76ED395 (user_id), INDEX IDX_E6C77495C0565BB5 (examplar_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE level (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(30) NOT NULL, created_at DATETIME NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE penality (id INT AUTO_INCREMENT NOT NULL, lease_id INT NOT NULL, user INT NOT NULL, UNIQUE INDEX UNIQ_D22CD012D3CA542C (lease_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE student (id INT AUTO_INCREMENT NOT NULL, level_id INT NOT NULL, first_name VARCHAR(50) NOT NULL, last_name VARCHAR(25) NOT NULL, born_at DATETIME NOT NULL, INDEX IDX_B723AF335FB14BA7 (level_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE user (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(60) NOT NULL, email VARCHAR(40) NOT NULL, password LONGTEXT NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE examplar ADD CONSTRAINT FK_68BB58D716A2B381 FOREIGN KEY (book_id) REFERENCES book (id)');
        $this->addSql('ALTER TABLE lease ADD CONSTRAINT FK_E6C77495CB944F1A FOREIGN KEY (student_id) REFERENCES student (id)');
        $this->addSql('ALTER TABLE lease ADD CONSTRAINT FK_E6C77495A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE lease ADD CONSTRAINT FK_E6C77495C0565BB5 FOREIGN KEY (examplar_id) REFERENCES examplar (id)');
        $this->addSql('ALTER TABLE penality ADD CONSTRAINT FK_D22CD012D3CA542C FOREIGN KEY (lease_id) REFERENCES lease (id)');
        $this->addSql('ALTER TABLE student ADD CONSTRAINT FK_B723AF335FB14BA7 FOREIGN KEY (level_id) REFERENCES level (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE examplar DROP FOREIGN KEY FK_68BB58D716A2B381');
        $this->addSql('ALTER TABLE lease DROP FOREIGN KEY FK_E6C77495C0565BB5');
        $this->addSql('ALTER TABLE penality DROP FOREIGN KEY FK_D22CD012D3CA542C');
        $this->addSql('ALTER TABLE student DROP FOREIGN KEY FK_B723AF335FB14BA7');
        $this->addSql('ALTER TABLE lease DROP FOREIGN KEY FK_E6C77495CB944F1A');
        $this->addSql('ALTER TABLE lease DROP FOREIGN KEY FK_E6C77495A76ED395');
        $this->addSql('DROP TABLE book');
        $this->addSql('DROP TABLE examplar');
        $this->addSql('DROP TABLE lease');
        $this->addSql('DROP TABLE level');
        $this->addSql('DROP TABLE penality');
        $this->addSql('DROP TABLE student');
        $this->addSql('DROP TABLE user');
    }
}
