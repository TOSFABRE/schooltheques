<?php

namespace App\Entity;

use App\Repository\ExamplarRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=ExamplarRepository::class)
 */
class Examplar
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=20)
     */
    private $isbn;

    /**
     * @ORM\ManyToOne(targetEntity=Book::class, inversedBy="examplars")
     * @ORM\JoinColumn(nullable=false)
     */
    private $book;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=Lease::class, mappedBy="examplar")
     */
    private $leases;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->leases = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getIsbn(): ?string
    {
        return $this->isbn;
    }

    public function setIsbn(string $isbn): self
    {
        $this->isbn = $isbn;

        return $this;
    }

    public function getBook(): ?Book
    {
        return $this->book;
    }

    public function setBook(?Book $book): self
    {
        $this->book = $book;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|Lease[]
     */
    public function getLeases(): Collection
    {
        return $this->leases;
    }

    public function addLease(Lease $lease): self
    {
        if (!$this->leases->contains($lease)) {
            $this->leases[] = $lease;
            $lease->setExamplar($this);
        }

        return $this;
    }

    public function removeLease(Lease $lease): self
    {
        if ($this->leases->contains($lease)) {
            $this->leases->removeElement($lease);
            // set the owning side to null (unless already changed)
            if ($lease->getExamplar() === $this) {
                $lease->setExamplar(null);
            }
        }

        return $this;
    }
}
