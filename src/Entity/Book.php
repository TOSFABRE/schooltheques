<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=40)
     */
    private $title;

    /**
     * @ORM\Column(type="string", length=60)
     */
    private $author;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\OneToMany(targetEntity=Examplar::class, mappedBy="book")
     */
    private $examplars;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->examplars = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    /**
     * @return Collection|Examplar[]
     */
    public function getExamplars(): Collection
    {
        return $this->examplars;
    }

    public function addExamplar(Examplar $examplar): self
    {
        if (!$this->examplars->contains($examplar)) {
            $this->examplars[] = $examplar;
            $examplar->setBook($this);
        }

        return $this;
    }

    public function removeExamplar(Examplar $examplar): self
    {
        if ($this->examplars->contains($examplar)) {
            $this->examplars->removeElement($examplar);
            // set the owning side to null (unless already changed)
            if ($examplar->getBook() === $this) {
                $examplar->setBook(null);
            }
        }

        return $this;
    }
}
