<?php

namespace App\Entity;

use App\Repository\LeaseRepository;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=LeaseRepository::class)
 */
class Lease
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="date")
     */
    private $begin;

    /**
     * @ORM\Column(type="date")
     */
    private $end;

    /**
     * @ORM\Column(type="datetime")
     */
    private $created_at;

    /**
     * @ORM\ManyToOne(targetEntity=Student::class, inversedBy="leases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $student;

    /**
     * @ORM\ManyToOne(targetEntity=User::class, inversedBy="leases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $user;

    /**
     * @ORM\ManyToOne(targetEntity=Examplar::class, inversedBy="leases")
     * @ORM\JoinColumn(nullable=false)
     */
    private $examplar;

    /**
     * @ORM\OneToOne(targetEntity=Penality::class, mappedBy="lease", cascade={"persist", "remove"})
     */
    private $penality;

    public function __construct()
    {
        $this->created_at = new \DateTime();
        $this->end = null;
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getBegin(): ?\DateTimeInterface
    {
        return $this->begin;
    }

    public function setBegin(\DateTimeInterface $begin): self
    {
        $this->begin = $begin;

        return $this;
    }

    public function getEnd(): ?\DateTimeInterface
    {
        return $this->end;
    }

    public function setEnd(\DateTimeInterface $end): self
    {
        $this->end = $end;

        return $this;
    }

    public function getCreatedAt(): ?\DateTimeInterface
    {
        return $this->created_at;
    }

    public function setCreatedAt(\DateTimeInterface $created_at): self
    {
        $this->created_at = $created_at;

        return $this;
    }

    public function getStudent(): ?Student
    {
        return $this->student;
    }

    public function setStudent(?Student $student): self
    {
        $this->student = $student;

        return $this;
    }

    public function getUser(): ?User
    {
        return $this->user;
    }

    public function setUser(?User $user): self
    {
        $this->user = $user;

        return $this;
    }

    public function getExamplar(): ?Examplar
    {
        return $this->examplar;
    }

    public function setExamplar(?Examplar $examplar): self
    {
        $this->examplar = $examplar;

        return $this;
    }

    public function getPenality(): ?Penality
    {
        return $this->penality;
    }

    public function setPenality(Penality $penality): self
    {
        $this->penality = $penality;

        // set the owning side of the relation if necessary
        if ($penality->getLease() !== $this) {
            $penality->setLease($this);
        }

        return $this;
    }
}
