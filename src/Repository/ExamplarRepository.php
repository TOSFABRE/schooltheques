<?php

namespace App\Repository;

use App\Entity\Examplar;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Examplar|null find($id, $lockMode = null, $lockVersion = null)
 * @method Examplar|null findOneBy(array $criteria, array $orderBy = null)
 * @method Examplar[]    findAll()
 * @method Examplar[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ExamplarRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Examplar::class);
    }

    // /**
    //  * @return Examplar[] Returns an array of Examplar objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('e.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Examplar
    {
        return $this->createQueryBuilder('e')
            ->andWhere('e.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */
}
