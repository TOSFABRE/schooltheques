<?php

namespace App\Form;

use App\Entity\Examplar;
use App\Entity\Lease;
use App\Entity\Student;
use phpDocumentor\Reflection\Type;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class LeaseType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('begin')
            ->add('student', EntityType::class, [
                'class' => Student::class,
                'choice_label' => function (Student $student) {
                    return $student->getFirstName() . ' ' . $student->getLastName();
                },
            ])
            ->add('examplar', EntityType::class, [
                'class' => Examplar::class,
                'choice_label' => function (Examplar $examplar) {
                    return $examplar->getBook()->getTitle() . ' Isbn: ' . $examplar->getIsbn();
                },
            ])
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Lease::class,
        ]);
    }
}
